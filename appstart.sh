#!/bin/bash

#This script exists to fulfil the requirement of a single command startup instead of doing "docker build -t webcont ./webcontainer && docker-compose up" 
#this script will allow the user to run "appstart.sh up" from the git repos root directory instead to build and deploy the stack.
source ./appcontainer.conf

buildimage() {
	sed -i 's/WEBURL/'$URL'/g' ./appcontainer/webapp.html
	docker build -t appcontainer ./appcontainer
}

deployimage() {
	sed -i 's/'$URL'/WEBURL/g' ./appcontainer/webapp.html
	docker-compose up &
}

stopimage() {
	docker-compose down
}

case $1 in
	up)
	buildimage
	deployimage
	;;
	down)
	stopimage
	;;
	*)
	printf "\n To run this script use <./appstart.sh up>  or <./appstart.sh down>\n\n"
	;;
esac