# Vibrato Tech test #

We would like you to demonstrate the automation of the spin-up and installation of a single-machine n-tier architecture. It is up to your interpretation which layers you would like to introduce, provided they can all be automatically started from a single command and serve some basic data. We require you to make a selection of technical choices you're most comfortable with, whether it be Linux vs. Windows or Java vs. Ruby. Choose the platforms, frameworks and languages you're most familiar with.

### Requirements ###

High-level: Using automation we want to spin up an environment which will allow us to connect to a web server on port 80 and serve a bit of simple HTML content from a data storage source. You will be required to write a small application in the language/framework of your choice to connect to the database, query it, and return the result to the user.

### What does it do ? ###

The app is a mock registration form for a website which will accept a firstname, lastname and email which is stored after submitting, later users cannot register with the same email addess.

![registration](https://i.imgur.com/A7wikeo.png)

![already in use](https://i.imgur.com/NUjjE48.png)


restarting the stack will clear the registration database. 


### Technology Used ###

Docker - Docker is used to build this application and docker-compose is used to deploy the docker container.

Alpine linux - Alpine linux is a security oriented lightweight linux distribution which was chosen due to its small size and portability

Python / Flask - Flask was used as the restful API framework for the http server, My main experience with writing api's has been with Python/Flask

Redis - Redis is used as the key value store for this web application, Redis was chosen as it is fast to implement and has a low setup overhead compared to SQL based databases, having experience in both I chose redis as in a real-world environment I would not run SQL inside a container. 

jQuery/Ajax - I have very little experience with writing front end web applications, prior to this project I researched a few different options, recently I have been taking a course in frontend web design so I had some idea of what I could do with jQuery,  After reading the documentation at https://api.jquery.com/jQuery.post/ I was able to fairly quickly put together a working ajax script in my HTML code. 

Nginx - In my work life I only use nginx for securing backend web applications and am more used to Apache and AWS Cloudfront however Nginx is designed to be a lighter web server, I selected Nginx as it seemed the most appropriate to use inside of a container. 

### To Run the app locally ###

To run this app you will need Docker, Docker compose and a system which supports "sed" (mac, linux or a windows compatible shell).

Both the container build and the deploy are built into a shell script to provide a full working demo, to run the script locally simply use

```
bash ./appstart.sh up
```

To tear down the stack use

```
bash ./appstart.sh down
```

You can now access this app via http://localhost/


#### To run from a public/private server ####

I have added a config option to allow this app to be deployed as-is to a public or private server, to do this

1. In appcontainer.conf in the root directory change the URL value to the IP or domain name of the machine you are deploying to. 

2. Run the start command specified above.  


### Who do I talk to? ###

Josh Hatfield - Author

