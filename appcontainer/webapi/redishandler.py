import redis

# Connect to default redis host
RedisServer = redis.StrictRedis(host='localhost', port=6379, db=0)


# checks if the email has been used and returns a response, using email as the main key

def CheckAndSubmit(UsrFName, UsrLName, UsrEmail):
    if RedisServer.get(UsrEmail + 'email') == UsrEmail:
        ExFName = RedisServer.get(UsrEmail + 'firstname')
        ExLName = RedisServer.get(UsrEmail + 'lastname')
        return ("%s is already in use by %s %s, Please register with another email." % (UsrEmail, ExFName, ExLName))
    else:
        RedisServer.set(UsrEmail + 'firstname', UsrFName)
        RedisServer.set(UsrEmail + 'lastname', UsrLName)
        RedisServer.set(UsrEmail + 'email', UsrEmail)
        return ("Thanks %s! your email %s has been registered." % (UsrFName, UsrEmail))
