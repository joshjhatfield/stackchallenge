from flask import Flask, request
from redishandler import CheckAndSubmit



App = Flask(__name__)


# Leaving in to check API is alive during testing
@App.route('/hello', methods=['GET'])
def hello():
    return "hello world"

# route for submission of data, posts to redis function
@App.route('/submit', methods=['POST'])
def submitdata():
    Fname = request.form['firstname']
    Lname = request.form['lastname']
    Email = request.form['email']
    return CheckAndSubmit(Fname, Lname, Email)

if __name__ == '__main__':
  App.run(debug=True)