#!/bin/sh
# Adding this script to run redis and python flask as background apps, the container does not like 3 non-daemonized commands being run simultaneously
# normall I would find a cleaner way to do this outside of a 3 hour window. 

redis-server &

python -m flask run --host=0.0.0.0 --port=8443 &